
TaskManager = function(){
    this.hash = {};
};
TaskManager.prototype.registerTask = function(name, func){
    if(this.hash[name]){
        throw new Error("Cannot register task with name = "+name+". It is already registered.")
        return;
    }
    this.hash[name] = func;
};

TaskManager.prototype.get = function(taskName){
    if(taskName == null){
        throw new Error("Cannot find null taskName. ");
        return;
    }
    if(this.hash[taskName] == null){
        throw new Error("Cannot get task = "+taskName+". It wasn't registered.");
        return;
    }
    return this.hash[taskName]
};
