Package.describe({
  summary: "TaskManager",
  version: "1.0.0"
});

Package.onUse(function(api) {
  api.versionsFrom('METEOR@0.9.3');
  api.addFiles('task-manager.js');
  api.export('TaskManager',['server','client']);
});

